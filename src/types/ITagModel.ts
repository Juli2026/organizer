export interface ITagModel {
    bg: string;
    color: string;
    id: string;
    name: string;

}
