export interface ILogin {
    data: string;
}

export interface ISignUpWithToken {
    data: any;
    name: string;
    email: string;
    token: string;
}
