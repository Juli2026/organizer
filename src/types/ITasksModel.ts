import { ITagModel } from './ITagModel';

export interface ITasksModel {
    id: string;
    completed?: boolean;
    title: string;
    description: string;
    deadline: Date;
    tag: ITagModel;
    created: string;
}


export interface ITasksResponse {
    data: ITasksModel[];
}

export interface ITaskResponse {
    data: ITasksModel;
}

export interface ITaskRequest {
    title: string;
    deadline: string;
    description: string;
    completed: boolean;
    tag: string;
}

