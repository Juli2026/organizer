import React from 'react';
import { SingUp } from '../components/SingUp/SingUp';
import { Nav } from '../components/Nav/Nav';

const SingUpPage: React.FC = () => {
    return (
        <div>
            <Nav />
            <SingUp />
        </div>
    );
};

export default SingUpPage;
