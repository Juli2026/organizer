import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { getToken } from '../lib/redux/selectors';

export const HomePages: React.FC = () => {
    const token = useSelector(getToken);
    const navigate = useNavigate();

    useEffect(() => {
        if (token) {
            navigate('/task-manager');
        } else {
            navigate('login');
        }
    }, [token]);

    return (
        <h1>Home Page</h1>
    );
};
