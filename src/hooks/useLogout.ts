import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useGetProfile } from './useGetProfile';

export const useLogout = () => {
    const navigate = useNavigate();

    const { setUserProfile } = useGetProfile();

    const token = localStorage.getItem('token');

    const setToken = (jwt: string) => {
        localStorage.setItem('token', jwt);
    };

    const removeToken = () => {
        setUserProfile(null);
        navigate('/login');
        localStorage.removeItem('token');
        toast.info('Возвращайся поскорее ;) Мы будем скучать');
    };

    return {
        token,
        setToken,
        removeToken,
    };
};
