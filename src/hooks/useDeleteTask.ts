import { useMutation } from 'react-query';
import { toast } from 'react-toastify';
import { api } from '../api';
import { ITasksModel } from '../types/ITasksModel';
import { useTasks } from './useTasks';

export const useDeleteTask = () => {
    const {
        taskList, updateTaskList, selectedTask, setSelectedTask, setTaskFormOpen,
    } = useTasks();

    const mutation = useMutation(
        (id: string) => api.tasks.deleteTask(id),
        {
            onSuccess() {
                const tasks = taskList?.filter((task: ITasksModel) => task.id !== selectedTask.id);

                updateTaskList(tasks);
                setSelectedTask(null);
                setTaskFormOpen(false);
                toast.info('Задача удалена!');
            },
        },
    );

    return mutation;
};
