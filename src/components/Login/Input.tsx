import { UseFormRegisterReturn } from 'react-hook-form';

export const Input: React.FC<IPropTypes> = (props) => {
    const input = (
        <input
            placeholder = { props.placeholder }
            type = { props.type }
            { ...props.register } />
    );

    return (
        <label>
            <div>
                <span className = 'errorMessage'>{ props.error?.message }</span>
            </div>
            { input }
        </label>
    );
};

Input.defaultProps = {
    type: 'text',
    tag:  'input',
};

interface IPropTypes {
    placeholder?: string;
    type?: string;
    tag?: string;
    register: UseFormRegisterReturn;
    error?: {
        message?: string;
    };
}
