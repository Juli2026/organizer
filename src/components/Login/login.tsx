import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Link } from 'react-router-dom';
import { schema } from './config';
import { Input } from './Input';
import { ILoginFormShape } from '../types';
import { useLogin } from '../../hooks/useLogin';
import '../../theme/styles/index.scss';

export const Login: React.FC = () => {
    const login = useLogin();

    const form  = useForm({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    const onSubmit = form.handleSubmit(async (credentials: ILoginFormShape) => {
        await login.mutateAsync(credentials);
        form.reset();
    });

    return (
        <main>
            <section className = 'sign-form'>
                <form onSubmit = { onSubmit } className = 'form'>
                    <fieldset>
                        <h1 className = 'legend'>Вход</h1>
                        <div className = 'inputBox'>
                            <Input
                                placeholder = 'Email'
                                error = { form.formState.errors.email }
                                register = { form.register('email') } />
                            <Input
                                placeholder = 'Пароль'
                                type = 'password'
                                error = { form.formState.errors.password }
                                register = { form.register('password') } />
                            <button type = 'submit' className = 'button-login'>Войти</button>
                        </div>
                    </fieldset>
                    <p className = 'loginBtn'>Если у вас до сих пор нет учётной записи, вы можете <Link to = '/singup' className = 'linkBtn'>зарегистрироваться.</Link></p>
                </form>
            </section>
        </main>

    );
};
