import { FC, MouseEvent } from 'react';

import { ITasksModel } from '../../../types/ITasksModel';
import { Head, CompleteTaskButton, RemoveTaskButton } from './taskControl.styled';

export interface ITaskControlTypeProps {
    selectedTask?: ITasksModel;
    handleTaskDelete?: (event: MouseEvent<HTMLDivElement>) => void;
    handleTaskComplete?: (event: MouseEvent<HTMLButtonElement>) => void;
}

export const TaskControl: FC<ITaskControlTypeProps> = (
    { selectedTask, handleTaskDelete, handleTaskComplete },
) => {
    return (
        <Head>
            { selectedTask
                && <CompleteTaskButton
                    disabled = { !selectedTask }
                    onClick = { handleTaskComplete }>Завершить</CompleteTaskButton>
            }

            { selectedTask && <RemoveTaskButton onClick = { handleTaskDelete }></RemoveTaskButton> }
        </Head>
    );
};
