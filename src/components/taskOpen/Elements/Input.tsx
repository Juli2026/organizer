import { UseFormRegisterReturn } from 'react-hook-form';

export const Input: React.FC<IPropTypes> = (props) => {
    const input = (
        <input
            placeholder = { props.placeholder }
            type = { props.type }
            { ...props.register } />
    );

    return (
        <div>
            <label>
                { input }
            </label>
        </div>

    );
};

export const TextArea: React.FC<IPropTypes> = (props) => {
    const textarea = (
        <textarea
            placeholder = { props.placeholder }
            { ...props.register } />
    );


    return (
        <div>
            <label>
                { textarea }
            </label>
        </div>

    );
};

Input.defaultProps = {
    type: 'text',
    tag:  'input',


};

TextArea.defaultProps = {
    tag: 'textarea',
};

interface IPropTypes {
    placeholder?: string;
    type?: string;
    tag?: string;
    register: UseFormRegisterReturn;
    error?: {
        message?: string;
    };
}
